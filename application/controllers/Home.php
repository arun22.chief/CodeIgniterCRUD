<?php

class Home extends CI_Controller{

	function index(){

		$data['page_title'] = 'Your title';
		$this->load->view('Layout/header',$data);

		$this->load->model("User_model");
		$users = $this->User_model->getUsers();
		echo var_dump($users);

		$dataToDo['todo_list'] = array('Clean House', 'Call Mom', 'Run Errands');
		$this->load->view('Home/index',$dataToDo);

		$this->load->view('Layout/footer');
	}
	
	function ShowCustomers(){
		$this->load->model('CrudModel');
		$records = $this->CrudModel->getRecords();
		// echo '<pre>'+print_r($records)+'</pre>';
	
		$this->load->view('Home/Show',['records'=>$records]);
	}


	function ShowList(){

		//multiple
		$query = $this->db->query('SELECT * FROM customer');

		foreach ($query->result_array() as $row)
		{
				echo $row['Name'];
				echo '<br>';
		}

		echo 'Total Results: ' . $query->num_rows();
		echo '<br>';
		echo '<br>';

		//single result
		$query = $this->db->query('SELECT Name FROM customer LIMIT 1');
		$row = $query->row_array();
		echo $row['Name'];

	}

	function InsertData(){

		$count = $this->db->count_all('customer');

		$sql = "INSERT INTO customer (Name, age) VALUES ('Test".$count."', 4)";
		$this->db->query($sql);
		echo $this->db->affected_rows();
	}

	function InsertQueryBuilder(){

		$count = $this->db->count_all('customer');

		$data = array(
			'Name' => "User".$count,
			'age' => 20
		);
	
		$this->db->insert('customer', $data);
		echo $this->db->affected_rows();
	}

	function UpdateCustomer(){
		$name = "UArun";
		$age = 29;

		$data = array('Name' => $name, 'age' => $age);


		$this->db->where('ID', 1);
		$this->db->update('customer', $data);

		echo $this->db->affected_rows();
	}

}

?>
